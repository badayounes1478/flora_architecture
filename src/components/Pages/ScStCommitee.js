import React, { useState } from 'react'
import Navigationbar from '../ReusableComponents/Navigation_bar'
import Fix_navigation_bar from '../ReusableComponents/Fix_navigation_bar'
import Footer from '../ReusableComponents/Footer'
import { Link } from 'react-router-dom'
import Info from '../../assets/commerce-and-shopping.svg'
import Social from '../../assets/social.svg'
import emailjs from 'emailjs-com';
import Slider from '../ReusableComponents/Slider'

function ScStCommitee() {
    const [flag, setflag] = useState(false)
    const [name, setname] = useState("")
    const [email, setemail] = useState("")
    const [contact, setcontact] = useState("")
    const [menu, setMenu] = useState(false)

    const showMenuBar = () => {
        setMenu(!menu)
    }


    const sendEmail = () => {

        var template_params = {
            "name": name,
            "email": email,
            "contact": contact
        }
        emailjs.send('default_service', 'template_QcFeJJkU', template_params, 'user_pMyeJVJzDOA6CDIxE2aM8')
            .then((result) => {
                alert('Form Submitted')
            }, (error) => {
                alert('Please try again')
            });
    }

    const showForm = () => {
        setflag(!flag)
    }

    return (
        <div>
            {
                menu === false ? null
                    : <Slider slide={showMenuBar} />
            }
            {
                flag === false ? <div class="circle" onClick={showForm}>
                    <img src={Info} alt="info" />
                </div> : <div className="form">
                        <div className="head">
                            <div></div>
                            <div>Admission Inquiry</div>
                            <div onClick={showForm} style={{ cursor: 'pointer' }}>X</div>
                        </div>
                        <div className="form-content">
                            <div className="icon-container">
                                <span className="icon">
                                    <img src={Social} alt="social" />
                                </span>
                                <span className="text-content">
                                    Hello! Welcome to Flora college
                              </span>
                            </div>
                            <div className="icon-container1">
                                <span className="icon">
                                    <img src={Social} alt="social" />
                                </span>
                                <span className="text-content" style={{ display: 'flex', flexDirection: 'column' }}>
                                    Please Tell a bit about yourself.
                              <div style={{ marginTop: 15, width: '70%' }}>
                                        <div>Your Name</div>
                                        <input onChange={(e) => setname(e.target.value)} type="text" placeholder="full name" />
                                    </div>
                                    <div style={{ marginTop: 15, width: '70%' }}>
                                        <div>Your E-mail Address</div>
                                        <input onChange={(e) => setemail(e.target.value)} type="text" placeholder="E-mail" />
                                    </div>
                                    <div style={{ marginTop: 15, width: '70%' }}>
                                        <div>Contact Number</div>
                                        <input onChange={(e) => setcontact(e.target.value)} type="text" placeholder="Contact Number" />
                                    </div>
                                </span>
                            </div>
                        </div>
                        <div onClick={sendEmail} className="submit">
                            SUBMIT
                   </div>
                    </div>
            }
            <Fix_navigation_bar />
            <Navigationbar slide={showMenuBar} />
            <div className="sc-main-container">

                <div class="sc-sub-nav">
                    <Link to="/antiraggingcomitee" class="inactive anti-sub-nav-item">Anti-Ragging Committee</Link>
                    <Link to="/vishakacommitee" class="inactive anti-sub-nav-item">Vishaka Judgement</Link>
                    <Link to="/internalcommitee" class="inactive anti-sub-nav-item">Internal Compliance Committee</Link>
                    <Link to="/scstcommitee" class="active anti-sub-nav-item">SC/ST Committee</Link>
                    <Link to="/grivance" class="inactive anti-sub-nav-item">Grievance Redressal Committee</Link>

                </div>
                <div className="sc-container">
                    <div className="sc-container-header">
                        <div>
                            SC/ST COMITTEE
                    </div>
                        <div className="sc-container-border">

                        </div>
                    </div>

                    <table>
                        <tr>
                            <th style={{ textAlign: 'center' }}>Sr. No.</th>
                            <th style={{ textAlign: 'center' }}>Name</th>
                            <th style={{ textAlign: 'center' }}>Contact</th>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>Ar. Rahuldev Patil</td>
                            <td>-</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Ar. Swapna Birajdar</td>
                            <td>-</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Ar. Prathamesh Joshi</td>
                            <td>-</td>
                        </tr>


                    </table>

                </div>
            </div>
            <Footer />
        </div>

    )
}

export default ScStCommitee 
