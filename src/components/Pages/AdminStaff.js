import React, { useState } from 'react'
import Navigationbar from '../ReusableComponents/Navigation_bar'
import Footer from '../ReusableComponents/Footer'
import { Link } from 'react-router-dom'
import Info from '../../assets/commerce-and-shopping.svg'
import Social from '../../assets/social.svg'
import emailjs from 'emailjs-com';
import Fix_navigation_bar from '../ReusableComponents/Fix_navigation_bar'
import Slider from '../ReusableComponents/Slider'

function AdminStaff() {
    const [flag, setflag] = useState(false)
    const [name, setname] = useState("")
    const [email, setemail] = useState("")
    const [contact, setcontact] = useState("")
    const [menu, setmenu] = useState(false)

    const showMenuBar = () => {
        setmenu(!menu)
    }


    const showForm = () => {
        setflag(!flag)
    }

    const sendEmail = () => {

        var template_params = {
            "name": name,
            "email": email,
            "contact": contact
        }
        emailjs.send('default_service', 'template_QcFeJJkU', template_params, 'user_pMyeJVJzDOA6CDIxE2aM8')
            .then((result) => {
                alert('Form Submitted')
            }, (error) => {
                alert('Please try again')
            });
    }


    return (
        <div>
            {
                menu === false ? null
                    : <Slider slide={showMenuBar} />
            }
            {
                flag === false ? <div class="circle" onClick={showForm}>
                    <img src={Info} alt="info" />
                </div> : <div className="form">
                        <div className="head">
                            <div></div>
                            <div>Admission Inquiry</div>
                            <div onClick={showForm} style={{ cursor: 'pointer' }}>X</div>
                        </div>
                        <div className="form-content">
                            <div className="icon-container">
                                <span className="icon">
                                    <img src={Social} alt="social" />
                                </span>
                                <span className="text-content">
                                    Hello! Welcome to Flora college
                              </span>
                            </div>
                            <div className="icon-container1">
                                <span className="icon">
                                    <img src={Social} alt="social" />
                                </span>
                                <span className="text-content" style={{ display: 'flex', flexDirection: 'column' }}>
                                    Please Tell a bit about yourself.
                              <div style={{ marginTop: 15, width: '70%' }}>
                                        <div>Your Name</div>
                                        <input onChange={(e) => setname(e.target.value)} type="text" placeholder="full name" />
                                    </div>
                                    <div style={{ marginTop: 15, width: '70%' }}>
                                        <div>Your E-mail Address</div>
                                        <input onChange={(e) => setemail(e.target.value)} type="text" placeholder="E-mail" />
                                    </div>
                                    <div style={{ marginTop: 15, width: '70%' }}>
                                        <div>Contact Number</div>
                                        <input onChange={(e) => setcontact(e.target.value)} type="text" placeholder="Contact Number" />
                                    </div>
                                </span>
                            </div>
                        </div>
                        <div onClick={sendEmail} className="submit">
                            SUBMIT
                   </div>
                    </div>
            }

            <Fix_navigation_bar />
            <Navigationbar slide={showMenuBar} />
            <div className="staff-sub-nav">
                <Link to="/faculty" className="inactive staff-sub-nav-item" >Faculty</Link>
                <Link to="/adminstaff" className="active staff-sub-nav-item" >Admin Staff</Link>
                <Link to="/staffactivity" className="inactive staff-sub-nav-item" >Staff Activity</Link>

            </div>
            <div className="admin-container">
                <div className="admin-account-container">
                    <div class="photo-container1">
                        <img className="admin-accountant-image" src={require('../../assets/Sanket Bhorde.jpg')} />
                    </div>
                    <div className="admin-account-name">
                        Mr. Sanket Borade
                        </div>
                    <div className="admin-account-designation">
                        Accountant
                        </div>
                    <div className="admin-account-qualification">
                        <p>-</p>
                    </div>
                    <div className="adminstaff-full-profile-button">
                        <a className="adminstaff-full-profile-header" href="">Full Profile</a>
                    </div>
                </div>
                <div className="admin-account-container">
                    <div class="photo-container1">
                        <img className="admin-accountant-image" src={require('../../assets/Lata More.jpg')} />
                    </div>
                    <div className="admin-account-name">
                        Mrs. Lata More
                        </div>
                    <div className="admin-account-designation">
                        Libarian
                        </div>
                    <div className="admin-account-qualification">
                        <p>-</p>
                    </div>
                    <div className="adminstaff-full-profile-button">
                        <a className="adminstaff-full-profile-header" href="">Full Profile</a>
                    </div>
                </div>

            </div>

            <Footer />
        </div>
    )
}

export default AdminStaff
