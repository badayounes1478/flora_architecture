import React, { useState } from 'react'
import Navigationbar from '../ReusableComponents/Navigation_bar'
import Footer from '../ReusableComponents/Footer'
import Info from '../../assets/commerce-and-shopping.svg'
import Social from '../../assets/social.svg'
import { Link } from 'react-router-dom'
import emailjs from 'emailjs-com';
import Fix_navigation_bar from '../ReusableComponents/Fix_navigation_bar'
import Slider from '../ReusableComponents/Slider'

const About = () => {
    const [flag, setflag] = useState(false)
    const [name, setname] = useState("")
    const [email, setemail] = useState("")
    const [contact, setcontact] = useState("")
    const [menu, setmenu] = useState(false)

    const sendEmail = () => {

        var template_params = {
            "name": name,
            "email": email,
            "contact": contact
        }
        emailjs.send('default_service', 'template_QcFeJJkU', template_params, 'user_pMyeJVJzDOA6CDIxE2aM8')
            .then((result) => {
                alert('Form Submitted')
            }, (error) => {
                alert('Please try again')
            });
    }


    const showForm = () => {
        setflag(!flag)
    }

    const showMenuBar = () => {
        setmenu(!menu)
    }

    return (
        <div>
            {
                menu === false ? null
                    : <Slider slide={showMenuBar} />
            }
            {
                flag === false ? <div class="circle" onClick={showForm}>
                    <img src={Info} alt="info" />
                </div> : <div className="form">
                        <div className="head">
                            <div></div>
                            <div>Admission Inquiry</div>
                            <div onClick={showForm} style={{ cursor: 'pointer' }}>X</div>
                        </div>
                        <div className="form-content">
                            <div className="icon-container">
                                <span className="icon">
                                    <img src={Social} alt="social" />
                                </span>
                                <span className="text-content">
                                    Hello! Welcome to Flora college
                              </span>
                            </div>
                            <div className="icon-container1">
                                <span className="icon">
                                    <img src={Social} alt="social" />
                                </span>
                                <span className="text-content" style={{ display: 'flex', flexDirection: 'column' }}>
                                    Please Tell a bit about yourself.
                              <div style={{ marginTop: 15, width: '70%' }}>
                                        <div>Your Name</div>
                                        <input onChange={(e) => setname(e.target.value)} type="text" placeholder="full name" />
                                    </div>
                                    <div style={{ marginTop: 15, width: '70%' }}>
                                        <div>Your E-mail Address</div>
                                        <input onChange={(e) => setemail(e.target.value)} type="text" placeholder="E-mail" />
                                    </div>
                                    <div style={{ marginTop: 15, width: '70%' }}>
                                        <div>Contact Number</div>
                                        <input onChange={(e) => setcontact(e.target.value)} type="text" placeholder="Contact Number" />
                                    </div>
                                </span>
                            </div>
                        </div>
                        <div onClick={sendEmail} className="submit">
                            SUBMIT
                   </div>
                    </div>
            }




            <Fix_navigation_bar />
            <Navigationbar slide={showMenuBar} />
            <div className="about-sub-nav">
                <Link to="/about" className="active about-sub-nav-item">About us</Link>
                <Link to="/vision" className="inactive about-sub-nav-item">Vision</Link>
                <Link to="/mission" className="inactive about-sub-nav-item">Mission</Link>
                <Link to="/notefromdirector" className="inactive about-sub-nav-item">About Director</Link>
                <Link to="/principlemessage" className="inactive about-sub-nav-item">Principal's Message</Link>
                <Link to="/governingcounsil" className="inactive about-sub-nav-item">Governing Council</Link>



            </div>
            <div className="about-border">
                <div className="about-title">
                    <strong>ABOUT FLORA</strong>
                    <div className="about-line"></div>
                </div>
                <p>In June, 2011, Rishikesh Kanase with his dad came to enroll as the first student at Flora Institutes. Ever since then, Flora Institutes has been fulfilling its role as a leading technical institute in Pune.<br /><br /> Flora is a family of academic institutions that include Schools, Colleges, and Skill Centres which have been imparting quality education for over 8 years. ’Flora’ word originates from the Latin word ‘Flor’ meaning flowers. Founder of the institute,
                     Dr.Atul Padalkar who has worked professionally as a
                     teacher over 35 years, thought this name apt for the
                     institution.<br /><br /> At Flora, every student is nurtured and
                     nourished as a flower so that one day they blossom
                     fragrance and become successful.
                     The campus epitomizes Flora’s vision, ‘Education for
                      a Sustainable Development'.<br /><br /> Flora Institutes, provides
                       fresh air and ample opportunities for the holistic
                       development of students who will be accomplished
                       individuals in many ways; happy and confident of
                       leading their lot to a brighter future.<br /><br /> It is a place
                       where brilliant minds assemble and collaborate,
                       where they pool together their talents across disciplines
                       in service of big projects and ideas. It’s a vibrant
                       community teaming up with students collaborating with
                       experts and specialists; a place of creativity and
                       innovation. <br /><br />It is an intersection of disciplines, a launching pad for a brilliant career, and a highly unique state of mind. It is a perfect environment to pursue your passion. Here the future is envisioned each day.  At Flora Institutes, we dedicate our efforts to that spirit of love of learning
                       and service toward the greater good. We are committed
                       to making a great education accessible to all by investing in
                       people and resources while holding the line on cost.<br /><br />
                       The Management solicits help, co-operation, generation of
                        new ideas from our Teaching &amp; Non- Teaching Staff,
                        Principals / Directors / Heads, Members of the General Body,
                         Parents, Past students and Well Wishers to help us achieve
                          this vision for Flora Institutes in the future.</p>
            </div>

            <Footer />
        </div>
    )
}

export default About
