import React,{useState,useEffect} from 'react'
import Navigationbar from '../ReusableComponents/Navigation_bar'
import Fix_navigation_bar from '../ReusableComponents/Fix_navigation_bar'
import Footer from '../ReusableComponents/Footer'
import { Link } from 'react-router-dom'
import Info from '../../assets/commerce-and-shopping.svg'
import Social from '../../assets/social.svg'
import emailjs from 'emailjs-com';
import Slider from '../ReusableComponents/Slider'

function Events() {
    useEffect(() => {
        window.scrollTo(0, 0)
      }, [])
    const [flag, setflag] = useState(false)
    const [name, setname] = useState("")
    const [email, setemail] = useState("")
    const [contact, setcontact] = useState("")
    const [menu, setmenu] = useState(false)

    const showMenuBar = () => {
        setmenu(!menu)
    }
  
    const sendEmail = () => {
        
        var template_params = {
            "name": name,
            "email": email,
            "contact": contact
        }
        emailjs.send('default_service', 'template_QcFeJJkU', template_params, 'user_pMyeJVJzDOA6CDIxE2aM8')
            .then((result) => {
                alert('Form Submitted')
            }, (error) => {
                alert('Please try again')
            });
    }
  

    const showForm = () => {
        setflag(!flag)
    }
  
    return (
        <div className="event-main-container">
            {
                menu === false ? null
                    : <Slider slide={showMenuBar} />
            }
              {
                    flag === false ? <div class="circle" onClick={showForm}>
                        <img src={Info} alt="info" />
                    </div> : <div className="form">
                            <div className="head">
                                <div></div>
                                <div>Admission Inquiry</div>
                                <div onClick={showForm} style={{ cursor: 'pointer' }}>X</div>
                            </div>
                            <div className="form-content">
                                <div className="icon-container">
                                    <span className="icon">
                                        <img src={Social} alt="social" />
                                    </span>
                                    <span className="text-content">
                                        Hello! Welcome to Flora college
                              </span>
                                </div>
                                <div className="icon-container1">
                                    <span className="icon">
                                        <img src={Social} alt="social" />
                                    </span>
                                    <span className="text-content" style={{ display: 'flex', flexDirection: 'column' }}>
                                        Please Tell a bit about yourself.
                              <div style={{ marginTop: 15, width: '70%' }}>
                                            <div>Your Name</div>
                                            <input onChange={(e)=>setname(e.target.value)} type="text" placeholder="full name" />
                                        </div>
                                        <div style={{ marginTop: 15, width: '70%' }}>
                                            <div>Your E-mail Address</div>
                                            <input onChange={(e)=>setemail(e.target.value)} type="text" placeholder="E-mail" />
                                        </div>
                                        <div style={{ marginTop: 15, width: '70%' }}>
                                            <div>Contact Number</div>
                                            <input onChange={(e)=>setcontact(e.target.value)} type="text" placeholder="Contact Number" />
                                        </div>
                                    </span>
                                </div>
                            </div>
                            <div onClick={sendEmail} className="submit">
                                SUBMIT
                   </div>
                        </div>
                }
           
            <Fix_navigation_bar />
            <Navigationbar slide={showMenuBar} />
            <div className="event-sub-nav">
            <Link className="active event-sub-nav-item" to="/events">Events</Link>
                <Link className="inactive event-sub-nav-item" to="/workshop">Workshops</Link>
                <Link className="inactive event-sub-nav-item" to="/studycase">Study tours & Case Studies</Link>
                <Link className="inactive event-sub-nav-item" to="/guestlecture">Guest Lectures</Link>
                <Link className="inactive event-sub-nav-item" to="/competition">Competitions</Link>
                <Link className=" inactive event-sub-nav-item" to="/annual">Annual College Exhibition</Link>
           
            </div>
            <div className="event-container">
                <div className="event-container-header">
                    <div>
                    Events
                    </div>
                    <div className="event-container-border">
    
                    </div>
                </div>
                <div className="event-card-header">
                1. Governing Council Visit
                </div>
                <div class="event-ICT-card-container">
                        <div class="event-ICT-card">
                            <img class="event-image" src={require('../../assets/governing council visit 1.jpg')} alt=""/>
                        </div>
                        <div class="event-ICT-card">
                        <img class="event-image" src={require('../../assets/governing council visit 2.JPG')} alt=""/>
                        </div>
                        <div class="event-ICT-card">
                        <img class="event-image" src={require('../../assets/governing council visit 3.JPG')} alt=""/>
                        </div>
                        <div class="event-ICT-card">
                        <img class="event-image" src={require('../../assets/governing council visit 4.JPG')} alt=""/>
                        </div>
                        <div class="event-ICT-card">
                        <img class="event-image" src={require('../../assets/governing council visit 5.JPG')} alt=""/>
                        </div>
                        <div class="event-ICT-card">
                        <img class="event-image" src={require('../../assets/governing council visit 6.jpg')} alt=""/>
                        </div>
                        <div class="event-ICT-card">
                        <img class="event-image" src={require('../../assets/governing council visit 7.jpg')} alt=""/>
                        </div>
                        <div class="event-ICT-card">
                        <img class="event-image" src={require('../../assets/governing council visit 8.jpg')} alt=""/>
                        
                        </div>
    
                </div>
    
                <div className="event-card-header">
                2. Cleanliness Drive
                </div>
                <div class="event-ICT-card-container">
                        <div class="event-ICT-card">
                        <img class="event-image" src={require('../../assets/cleanliness 1.JPG')} alt=""/>
                        
                        </div>
                        <div class="event-ICT-card">
                        <img class="event-image" src={require('../../assets/cleanliness 2.JPG')} alt=""/>
                       
                        </div>
                        <div class="event-ICT-card">
                        <img class="event-image" src={require('../../assets/cleanliness 3.JPG')} alt=""/>
                       
                        </div>
                        <div class="event-ICT-card">
                        <img class="event-image" src={require('../../assets/cleanliness 4.JPG')} alt=""/>
                       
                        </div>
                        <div class="event-ICT-card">
                        <img class="event-image" src={require('../../assets/cleanliness 5.JPG')} alt=""/>
                       
                        </div>
                        <div class="event-ICT-card">
                        <img class="event-image" src={require('../../assets/cleanliness 6.JPG')} alt=""/>
                       
                        </div>
    
                </div>

                <div className="event-card-header">
                3. Excellentia
                </div>
                <div class="event-ICT-card-container">
                        <div class="event-ICT-card">
                            <img class="event-image" src={require('../../assets/excellentia 1.JPG')} alt=""/>
                        </div>
                        <div class="event-ICT-card">
                        <img class="event-image" src={require('../../assets/excellentia 2.jpg')} alt=""/>
                        
                        </div>
                        <div class="event-ICT-card">
                        <img class="event-image" src={require('../../assets/excellentia 3.JPG')} alt=""/>
                        
                        </div>
                        <div class="event-ICT-card">
                        <img class="event-image" src={require('../../assets/excellentia 4.JPG')} alt=""/>
                        
                        </div>
    
                </div>
    
                <div className="event-card-header">
                4. Zonasa
                </div>
                <div class="event-ICT-card-container">
                        <div class="event-ICT-card">
                        <img class="event-image" src={require('../../assets/zonasa 1.jpg')} alt=""/>
                      
                        </div>
                        <div class="event-ICT-card">
                        <img class="event-image" src={require('../../assets/zonasa 2.jpg')} alt=""/>
                     
                        </div>
                        <div class="event-ICT-card">
                        <img class="event-image" src={require('../../assets/zonasa 3.jpg')} alt=""/>
                     
                        </div>
                        <div class="event-ICT-card">
                        <img class="event-image" src={require('../../assets/zonasa 4.jpg')} alt=""/>
                     
                        </div>
                        <div class="event-ICT-card">
                        <img class="event-image" src={require('../../assets/zonasa 5.jpg')} alt=""/>
                      
                        </div>
                        <div class="event-ICT-card">
                        <img class="event-image" src={require('../../assets/zonasa 6.jpg')} alt=""/>
                    
                        </div>
                        <div class="event-ICT-card">
                        <img class="event-image" src={require('../../assets/zonasa 7.jpg')} alt=""/>
                     
                        </div>
                        <div class="event-ICT-card">
                        <img class="event-image" src={require('../../assets/zonasa 8.jpg')} alt=""/>
                     
                        </div>
    
                </div>
    
            </div>
            <Footer/>
        </div>
    )
}

export default Events
