import React, { useState } from 'react'
import Navigationbar from '../ReusableComponents/Navigation_bar'
import Footer from '../ReusableComponents/Footer'
import Info from '../../assets/commerce-and-shopping.svg'
import Social from '../../assets/social.svg'
import { Link } from 'react-router-dom'
import emailjs from 'emailjs-com';
import Slider from '../ReusableComponents/Slider'
import Fix_navigation_bar from '../ReusableComponents/Fix_navigation_bar'


const Notefromdirector = () => {
    const [flag, setflag] = useState(false)
    const [name, setname] = useState("")
    const [email, setemail] = useState("")
    const [contact, setcontact] = useState("")
    const [menu, setmenu] = useState(false)

    const showMenuBar = () => {
        setmenu(!menu)
    }

    const sendEmail = () => {

        var template_params = {
            "name": name,
            "email": email,
            "contact": contact
        }
        emailjs.send('default_service', 'template_QcFeJJkU', template_params, 'user_pMyeJVJzDOA6CDIxE2aM8')
            .then((result) => {
                alert('Form Submitted')
            }, (error) => {
                alert('Please try again')
            });
    }


    const showForm = () => {
        setflag(!flag)
    }

    return (
        <div>
            {
                menu === false ? null
                    : <Slider slide={showMenuBar} />
            }
            {
                flag === false ? <div class="circle" onClick={showForm}>
                    <img src={Info} alt="info" />
                </div> : <div className="form">
                        <div className="head">
                            <div></div>
                            <div>Admission Inquiry</div>
                            <div onClick={showForm} style={{ cursor: 'pointer' }}>X</div>
                        </div>
                        <div className="form-content">
                            <div className="icon-container">
                                <span className="icon">
                                    <img src={Social} alt="social" />
                                </span>
                                <span className="text-content">
                                    Hello! Welcome to Flora college
                              </span>
                            </div>
                            <div className="icon-container1">
                                <span className="icon">
                                    <img src={Social} alt="social" />
                                </span>
                                <span className="text-content" style={{ display: 'flex', flexDirection: 'column' }}>
                                    Please Tell a bit about yourself.
                              <div style={{ marginTop: 15, width: '70%' }}>
                                        <div>Your Name</div>
                                        <input onChange={(e) => setname(e.target.value)} type="text" placeholder="full name" />
                                    </div>
                                    <div style={{ marginTop: 15, width: '70%' }}>
                                        <div>Your E-mail Address</div>
                                        <input onChange={(e) => setemail(e.target.value)} type="text" placeholder="E-mail" />
                                    </div>
                                    <div style={{ marginTop: 15, width: '70%' }}>
                                        <div>Contact Number</div>
                                        <input onChange={(e) => setcontact(e.target.value)} type="text" placeholder="Contact Number" />
                                    </div>
                                </span>
                            </div>
                        </div>
                        <div onClick={sendEmail} className="submit">
                            SUBMIT
                   </div>
                    </div>
            }

            <Fix_navigation_bar />
            <Navigationbar slide={showMenuBar} />
            <div className="about-sub-nav">
                <Link to="/about" className="inactive about-sub-nav-item">About us</Link>
                <Link to="/vision" className="inactive about-sub-nav-item">Vision</Link>
                <Link to="/mission" className="inactive about-sub-nav-item">Mission</Link>
                <Link to="/notefromdirector" className="active about-sub-nav-item">About Director</Link>
                <Link to="/principlemessage" className="inactive about-sub-nav-item">Principal's Message</Link>
                <Link to="/governingcounsil" className="inactive about-sub-nav-item">Governing Council</Link>



            </div>

            <div className="note-from-director-conteiner">
                <div className="note-from-director-title">
                    <strong>ABOUT DIRECTOR</strong>
                    <div className="about-line"></div>
                </div>
                <div className="note-from-director-grid">
                    <div className="note-from-director-image">
                        <img class="note-img" src={require('../../assets/director.png')} />
                    </div>
                    <div className="note-from-director-content">
                        <div className="note-from-director-sub-title">Dr. Atul Padalkar</div>
                        <p>
                            He obtained his degree in BE and ME in Mechanical Engineering. He has done a Ph.D. in the area of ‘Alternative Refrigerants’ under the guidance of Dr. Devotta. Dr. Devotta had been associated with work which was recognized for the award of Noble prize to IPCC in 2007. <br /><br />He started his career in 1987 as a Lecture at Rajarambapu Instituteof Technology. Subsequently, he has served various prestigious institutions incapacity of Dean, Principal, Professor, and Assistant Professor in Savitribai Phule Pune University (SPPU), Sinhgad College of Engineering, Pune (2004-2011) AISSMS College of Engineering, Pune, (1996 - 2003) respectively are important.<br /><br /> He has been instrumental in starting the Technology department at Savitribai Phule Pune University.
                        </p>
                    </div>
                </div>
                <div className="bottom-paragraph">
                    <p>He has been a mentor for introducing various academic programs which include B.tech Biotechnology , Postgraduate programs in Mechatronics, Computer Networks, VLSI & Embedded Systems, and Communication Networks for SPPU. </p><br /><br />
                    <p>He has associated actively with key positions at various technological Institutions and technical bodies like Indian Science Congress, NationalInstitute of Virology, Pune, ASHRAE, ISHRAE, Institution of Engineers (I) and Indian Society for Technical Education (ISTE), New Delhi as Advisor, Member, and Life Member. He has also worked as the Chairman, ISTE Maharashtra & Goa, and president, Engineering Sciences, Indian Science Congress.</p><br /><br />
                    <p>His contributions particularly the development of room air conditioners with HC-290 and handbook on good servicing of room air conditioners under the aegis of German Technical Cooperation (GIZ), Germany has been recognised widely in the field of Engineering and Industrial Research.<br /><br /> The work of the development of hydrocarbon technology in air conditioners has been appreciated by UNEP and the International Institute of Refrigeration (IIR). He has worked as a consultant for corporate houses like GIZ, Germany, Blue Star, Thermax Ltd., Godrej Appliances Ltd., India. </p><br /><br />
                    <p>He provides technicalsupport for the design and development of products to business houses. He has attended more than a score of national and international conferences and workshops. He has been honouredby the 24th International Congress of Refrigeration inviting him to chair two sessions in ICR2015, Japan. He has published more than seventy research papers in the conference proceedings, bulletins, and journals of national and international repute. </p><br /><br />
                    <p>The important publications include “Performance Assessment of Air Conditioners Using Propane” appeared in Official Journal of Centro Studi Galileo (CSG) and European Energy Center (EEC), Special International Issue (ISI), UNEP, 2010.The article entitled “Performance Assessment of Air Conditioners with HC290” appeared in the Bulletin of the IIR, 2010 is another landmark in his achievements. The same work has been published in Spanish and French languages.</p>
                </div>
            </div>
            <Footer />
        </div>
    )
}

export default Notefromdirector
