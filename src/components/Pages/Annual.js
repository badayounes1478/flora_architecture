import React, { useState } from 'react'
import Navigationbar from '../ReusableComponents/Navigation_bar'
import Fix_navigation_bar from '../ReusableComponents/Fix_navigation_bar'
import Footer from '../ReusableComponents/Footer'
import { Link } from 'react-router-dom'
import Info from '../../assets/commerce-and-shopping.svg'
import Social from '../../assets/social.svg'
import emailjs from 'emailjs-com';
import Slider from '../ReusableComponents/Slider'

function Annual() {
    const [flag, setflag] = useState(false)
    const [name, setname] = useState("")
    const [email, setemail] = useState("")
    const [contact, setcontact] = useState("")
    const [menu, setmenu] = useState(false)

    const showMenuBar = () => {
        setmenu(!menu)
    }

    const sendEmail = () => {

        var template_params = {
            "name": name,
            "email": email,
            "contact": contact
        }
        emailjs.send('default_service', 'template_QcFeJJkU', template_params, 'user_pMyeJVJzDOA6CDIxE2aM8')
            .then((result) => {
                alert('Form Submitted')
            }, (error) => {
                alert('Please try again')
            });
    }
    const showForm = () => {
        setflag(!flag)
    }



    return (
        <div className="annual-main-container">
            {
                menu === false ? null
                    : <Slider slide={showMenuBar} />
            }
            {
                flag === false ? <div class="circle" onClick={showForm}>
                    <img src={Info} alt="info" />
                </div> : <div className="form">
                        <div className="head">
                            <div></div>
                            <div>Admission Inquiry</div>
                            <div onClick={showForm} style={{ cursor: 'pointer' }}>X</div>
                        </div>
                        <div className="form-content">
                            <div className="icon-container">
                                <span className="icon">
                                    <img src={Social} alt="social" />
                                </span>
                                <span className="text-content">
                                    Hello! Welcome to Flora college
                              </span>
                            </div>
                            <div className="icon-container1">
                                <span className="icon">
                                    <img src={Social} alt="social" />
                                </span>
                                <span className="text-content" style={{ display: 'flex', flexDirection: 'column' }}>
                                    Please Tell a bit about yourself.
                              <div style={{ marginTop: 15, width: '70%' }}>
                                        <div>Your Name</div>
                                        <input onChange={(e) => setname(e.target.value)} type="text" placeholder="full name" />
                                    </div>
                                    <div style={{ marginTop: 15, width: '70%' }}>
                                        <div>Your E-mail Address</div>
                                        <input onChange={(e) => setemail(e.target.value)} type="text" placeholder="E-mail" />
                                    </div>
                                    <div style={{ marginTop: 15, width: '70%' }}>
                                        <div>Contact Number</div>
                                        <input onChange={(e) => setcontact(e.target.value)} type="text" placeholder="Contact Number" />
                                    </div>
                                </span>
                            </div>
                        </div>
                        <div onClick={sendEmail} className="submit">
                            SUBMIT
                   </div>
                    </div>
            }

            <Fix_navigation_bar />
            <Navigationbar slide={showMenuBar} />
            <div className="annual-sub-nav">
                <Link className="inactive annual-sub-nav-item" to="/events">Events</Link>
                <Link className="inactive annual-sub-nav-item" to="/workshop">Workshops</Link>
                <Link className="inactive annual-sub-nav-item" to="/studycase">Study tours & Case Studies</Link>
                <Link className="inactive annual-sub-nav-item" to="/guestlecture">Guest Lectures</Link>
                <Link className="inactive annual-sub-nav-item" to="/competition">Competitions</Link>
                <Link className="active annual-sub-nav-item" to="/annual">Annual College Exhibition</Link>

            </div>
            <div className="annual-container">
                <div className="annual-container-header">
                    <div>
                        Annual College Exhibition
                    </div>
                    <div className="annual-container-border">

                    </div>
                </div>
                <div className="annual-container-paragraph">
                    <strong>ANNUAL COLLEGE EXHIBITION (1st March- 4th March 2020)</strong><br /><br /> We, the Florians just completed 3 years in 2020. Yet, we successfully managed to hold an Annual Exhibition at Yashwantrao Chavan Art Gallery. This was our very first exhibition and we received a great response from all the visitors, so much that many people requested us to extend the initially planned 3-day exhibition for another day.<br /><br /> It was a 4-day event and we all put our best efforts to showcase our student's academic work as well as other creative art forms like photos, sketches and paintings.<br /><br /> The event also included various workshops like canvas painting, sketching puzzle corner and many more, as well as presentations for all the visitors and the visitors, were excited to participate. All in all, the exhibition was a huge success and we look forward to our next exhibition which will happen in 2021!
               </div>
                <div class="annual-ICT-card-container">
                    <div class="annual-ICT-card">
                        <img className="annual-image" src={require('../../assets/art exhibition 0.JPG')} alt="" />
                    </div>
                    <div class="annual-ICT-card">
                        <img className="annual-image" src={require('../../assets/art exhibition 1.JPG')} alt="" />

                    </div>
                    <div class="annual-ICT-card">
                        <img className="annual-image" src={require('../../assets/art exhibition 2.JPG')} alt="" />

                    </div>
                    <div class="annual-ICT-card">
                        <img className="annual-image" src={require('../../assets/art exhibition 3.JPG')} alt="" />

                    </div>
                    <div class="annual-ICT-card">
                        <img className="annual-image" src={require('../../assets/art exhibition 4.JPG')} alt="" />

                    </div>
                    <div class="annual-ICT-card">
                        <img className="annual-image" src={require('../../assets/art exhibition 5.JPG')} alt="" />

                    </div>
                    <div class="annual-ICT-card">
                        <img className="annual-image" src={require('../../assets/art exhibition 6.JPG')} alt="" />

                    </div>
                    <div class="annual-ICT-card">
                        <img className="annual-image" src={require('../../assets/art exhibition 7.JPG')} alt="" />

                    </div>
                </div>
            </div>
            <Footer />
        </div>


    )
}

export default Annual
