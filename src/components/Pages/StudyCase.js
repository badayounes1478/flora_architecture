import React, { useState, useEffect } from 'react'
import Navigationbar from '../ReusableComponents/Navigation_bar'
import Fix_navigation_bar from '../ReusableComponents/Fix_navigation_bar'
import Footer from '../ReusableComponents/Footer'
import { Link } from 'react-router-dom'
import Info from '../../assets/commerce-and-shopping.svg'
import Social from '../../assets/social.svg'
import emailjs from 'emailjs-com';
import Slider from '../ReusableComponents/Slider'


function StudyCase() {
    useEffect(() => {
        window.scrollTo(0, 0)
    }, [])

    const [flag, setflag] = useState(false)
    const [name, setname] = useState("")
    const [email, setemail] = useState("")
    const [contact, setcontact] = useState("")
    const [menu, setmenu] = useState(false)

    const showMenuBar = () => {
        setmenu(!menu)
    }

    const sendEmail = () => {

        var template_params = {
            "name": name,
            "email": email,
            "contact": contact
        }
        emailjs.send('default_service', 'template_QcFeJJkU', template_params, 'user_pMyeJVJzDOA6CDIxE2aM8')
            .then((result) => {
                alert('Form Submitted')
            }, (error) => {
                alert('Please try again')
            });
    }


    const showForm = () => {
        setflag(!flag)
    }

    return (
        <div className="study-main-container">
            {
                menu === false ? null
                    : <Slider slide={showMenuBar} />
            }
            {
                flag === false ? <div class="circle" onClick={showForm}>
                    <img src={Info} alt="info" />
                </div> : <div className="form">
                        <div className="head">
                            <div></div>
                            <div>Admission Inquiry</div>
                            <div onClick={showForm} style={{ cursor: 'pointer' }}>X</div>
                        </div>
                        <div className="form-content">
                            <div className="icon-container">
                                <span className="icon">
                                    <img src={Social} alt="social" />
                                </span>
                                <span className="text-content">
                                    Hello! Welcome to Flora college
                              </span>
                            </div>
                            <div className="icon-container1">
                                <span className="icon">
                                    <img src={Social} alt="social" />
                                </span>
                                <span className="text-content" style={{ display: 'flex', flexDirection: 'column' }}>
                                    Please Tell a bit about yourself.
                              <div style={{ marginTop: 15, width: '70%' }}>
                                        <div>Your Name</div>
                                        <input onChange={(e) => setname(e.target.value)} type="text" placeholder="full name" />
                                    </div>
                                    <div style={{ marginTop: 15, width: '70%' }}>
                                        <div>Your E-mail Address</div>
                                        <input onChange={(e) => setemail(e.target.value)} type="text" placeholder="E-mail" />
                                    </div>
                                    <div style={{ marginTop: 15, width: '70%' }}>
                                        <div>Contact Number</div>
                                        <input onChange={(e) => setcontact(e.target.value)} type="text" placeholder="Contact Number" />
                                    </div>
                                </span>
                            </div>
                        </div>
                        <div onClick={sendEmail} className="submit">
                            SUBMIT
                   </div>
                    </div>
            }

            <Fix_navigation_bar />
            <Navigationbar slide={showMenuBar} />
            <div className="study-sub-nav">
                <Link className="inactive study-sub-nav-item" to="/events">Events</Link>
                <Link className="inactive study-sub-nav-item" to="/workshop">Workshops</Link>
                <Link className="active study-sub-nav-item" to="/studycase">Study tours & Case Studies</Link>
                <Link className="inactive study-sub-nav-item" to="/guestlecture">Guest Lectures</Link>
                <Link className="inactive study-sub-nav-item" to="/competition">Competitions</Link>
                <Link className="inactive study-sub-nav-item" to="/annual">Annual College Exhibition</Link>

            </div>
            <div className="study-container">
                <div className="study-container-header">
                    <div>
                        Study tours & Case Studies
                    </div>
                    <div className="study-container-border">

                    </div>
                </div>
                <div className="study-container-paragraph">
                    <strong>1. Jala Srushti:</strong><br />  Site visit at Jala Srushti gave students a wonderful experience on the practical implementation of art, design, style and techniques in architecture.<br /><br />
                </div>
                <div class="study-ICT-card-container">
                    <div class="study-ICT-card">
                        <img className="studycase-image" src={require('../../assets/jala srushti 1.JPG')} alt="" />
                    </div>
                    <div class="study-ICT-card">
                        <img className="studycase-image" src={require('../../assets/jala srushti 2.JPG')} alt="" />

                    </div>
                    <div class="study-ICT-card">
                        <img className="studycase-image" src={require('../../assets/jala srushti 3.JPG')} alt="" />

                    </div>
                    <div class="study-ICT-card">
                        <img className="studycase-image" src={require('../../assets/jala srushti 4.JPG')} alt="" />

                    </div>
                    <div class="study-ICT-card">
                        <img className="studycase-image" src={require('../../assets/jala srushti 5.JPG')} alt="" />

                    </div><div class="study-ICT-card">
                        <img className="studycase-image" src={require('../../assets/jala srushti 6.JPG')} alt="" />

                    </div><div class="study-ICT-card">
                        <img className="studycase-image" src={require('../../assets/jala srushti 7.JPG')} alt="" />

                    </div><div class="study-ICT-card">
                        <img className="studycase-image" src={require('../../assets/jala srushti 8.JPG')} alt="" />

                    </div>
                    <div class="study-ICT-card">
                        <img className="studycase-image" src={require('../../assets/jala srushti 9.JPG')} alt="" />

                    </div>
                </div>
                <div className="study-container-paragraph">
                    <strong>2. Harappan Civilisation:<br /></strong>  7 days tour to the origin of Harappan civilisation (Bhuj - Gujarat) Guidance on restoration by Ar. Anjali Kalamdani and Ar. Kiran Kalamdani Settlement study tour Menavali (Wai)<br /><br />
                </div>
                <div class="study-ICT-card-container">
                    <div class="study-ICT-card">
                        <img className="studycase-image" src={require('../../assets/harappa 1.jpg')} alt="" />

                    </div>
                    <div class="study-ICT-card">
                        <img className="studycase-image" src={require('../../assets/harappa 2.jpg')} alt="" />

                    </div>
                    <div class="study-ICT-card">
                        <img className="studycase-image" src={require('../../assets/harappa 3.jpg')} alt="" />

                    </div>
                    <div class="study-ICT-card">
                        <img className="studycase-image" src={require('../../assets/harappa 4.jpg')} alt="" />

                    </div>
                    <div class="study-ICT-card">
                        <img className="studycase-image" src={require('../../assets/harappa 5.jpg')} alt="" />

                    </div>
                </div>
                <div className="study-container-paragraph">
                    <strong>3. North Karnataka:</strong><br />  Students of second-year Architecture explored evolution on of south Indian Architecture through study tour at Hampi, Badami and Bijapur in November 2018.<br /><br />
                </div>
                <div class="study-ICT-card-container">
                    <div class="study-ICT-card">
                        <img className="studycase-image" src={require('../../assets/north karnataka 1.png')} alt="" />

                    </div>
                    <div class="study-ICT-card">
                        <img className="studycase-image" src={require('../../assets/north karnataka 2.jpg')} alt="" />

                    </div>

                </div>
                <div className="study-container-paragraph">
                    <strong> 4. Ajanta-Ellora:</strong><br /> First-year students learnt historic Buddhist and Jain style Architecture during the visit of heritage sites at Ajanta, Ellora and Aurangabad in January 2019.
              </div>
                <div class="study-ICT-card-container">
                    <div class="study-ICT-card">
                        <img className="studycase-image" src={require('../../assets/ajanta ellora.png')} alt="" />

                    </div>

                </div>

            </div>
            <Footer />
        </div>

    )
}

export default StudyCase
