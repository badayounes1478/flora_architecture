import React from 'react'
import next from '../../assets/back3.svg'
import { Link } from 'react-router-dom'

const Button = (props) => {
    return (
        <div className="button">
            <Link style={{color:'black'}} to={props.to}>{props.title}</Link>
            <span>
                <img src={next} alt="next" />
            </span>
        </div>
    )
}



export default Button
