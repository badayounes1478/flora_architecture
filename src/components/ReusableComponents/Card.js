import React from 'react'

const Card = (props) => {
    return (
        <div style={{display:'flex', marginLeft:'12%', marginTop:15}}>
            <div className="box">
              <div style={{marginBottom:'15%'}}>20</div>
              <div>MAY</div>
            </div>
            <div className="box-content">
    <p>{props.eventname}</p>
    <span>Venue: {props.venue}</span>
            </div>
        </div>
    )
}

export default Card
