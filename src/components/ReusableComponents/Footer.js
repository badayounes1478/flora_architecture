import React, { useState } from 'react'
import facebook from '../../assets/facebook-3.svg'
import instagram from '../../assets/instagram-2-1.svg'
import twitter from '../../assets/twitter.svg'
import linkedin from '../../assets/linkedin-icon.svg'
import copyright from '../../assets/copyright.svg'
import { Link } from 'react-router-dom'
import ReactModal from 'react-modal';


const Footer = () => {
    const [isopen, setisopen] = useState(false)

    const toggleModal = () => {
        setisopen(!isopen)
    }

    return (
        <React.Fragment>
            <footer id="footer">
                <ReactModal
                    isOpen={isopen}
                    contentLabel="onRequestClose Example"
                    onRequestClose={toggleModal}
                    shouldCloseOnOverlayClick={true}
                >
                    <div className="modal-container">
                        <div className="cross" onClick={toggleModal}>X</div>
                        <div className="map-image"></div>
                    </div>
                </ReactModal>
                <div class="grid3">
                    <div>
                        <strong>ABOUT FCOA</strong>
                        <p>At FCOA, you will have amazing opportunities to explore architectural work by studying it in a friendly and professional environment using modern equipment and resources.</p>
                    </div>
                    <div class="center"><strong>QUICK LINKS</strong>
                        <div class="link">
                            <span>
                                <a href="/">Home</a>
                            </span>
                            <span>
                                <a href="/faculty">Faculty</a>
                            </span>
                            <span>
                                <a href="/news">News</a>
                            </span>
                            <span>
                                <a href="/events">Events</a>
                            </span>
                            <span>
                                <a href="/barch">Admission</a>
                            </span>
                            <span>
                                <a href="/contact">Contact</a>
                            </span>
                            <span
                                class="no-border"><a href="https://flora.ac.in">Back To Home</a>
                            </span>
                        </div>
                    </div>
                    <div>
                        <strong>GET IN TOUCH</strong>
                        <div class="getintouch">
                            <p>Flora Institutes 49/1, Pune, Khed-Shivapur Pune Landmark: Near Khed-Shivapur Toll Plaza</p>
                            <p>Contact no-</p>
                            <p>9890-673-701</p>
                            <p>7030-719-401</p>
                            <p>7978-513-867</p>
                            <p>Map Location :</p>
                            <p>
                                <a target="_blank" rel="noopener noreferrer" href="https://goo.gl/maps/TufTaivYWZqJ47CR8" style={{ color: 'rgb(202, 202, 202)' }}>https://goo.gl/maps/TufTaivYWZqJ47CR8</a>
                            </p>
                        </div>
                        <div class="icons">
                            <a href="https://www.facebook.com/flora.architecture/">
                                <img src="/static/media/facebook-3.6e4b8f1b.svg" alt="facebook" />
                            </a>
                            <a href="https://www.instagram.com/wetheflorians/">
                                <img src="/static/media/instagram-2-1.4e83cb13.svg" alt="instagram" />
                            </a>
                            <a
                                href="https://twitter.com/FloraInstitutes?lang=en">
                                <img src="/static/media/twitter.af28d1ac.svg" alt="twitter" /></a>
                            <a href="https://www.linkedin.com/company/flora-college-of-architecture-pune/">
                                <img src="/static/media/linkedin-icon.87d92f54.svg" alt="linkedin" />
                            </a>
                            <a href="https://in.pinterest.com/floraeduschool/">
                                <img src="/static/media/pinterest.9bb54872.svg" alt="pinterest" />
                            </a>
                        </div>
                    </div>
                    <div class="reach-us-container"><strong>REACH US OUT</strong>
                        <div class="reachus" onClick={toggleModal}></div>
                    </div>
                </div>
            </footer>
            <div className="copyright">
                <img src={copyright} alt="copyright" />
                <strong>Flora institute 2020</strong>
            </div>

        </React.Fragment>
    )
}

export default React.memo(Footer)
